const _ = require("lodash");
const {loadFields} = require("../../utils");

function retrieveTypeDependantFields({z, api, bundle: {inputData: {type}}, fieldKeyPrefix, fieldFilterFn = _.identity}) {
  if (!type) {
    return Promise.resolve([]);
  }
  return api.fetchSchema()
    .then((schema) => loadFields({
      z,
      typeObjects: schema.typeObjects,
      type,
      api,
      includeChoices: true,
      fieldKeyPrefix,
      fieldFilterFn,
    }));
}

const makeTypeDependantFieldsRetriever = ({fieldKeyPrefix, fieldFilterFn}) => {
  return (args) => retrieveTypeDependantFields(Object.assign({fieldKeyPrefix, fieldFilterFn}, args));
};

module.exports = {retrieveTypeDependantFields, makeTypeDependantFieldsRetriever};
