const {
  getSelectWithSupportedFields,
  findTypeObjectByIdOrName,
  provideZapierId,
  withRichTextFieldsDocumentsLoaded,
  withObjectLink,
} = require("../../utils");

const PAGE_LIMIT = 100;

async function performListCreatedOperation({api, bundle: {inputData: {type}, meta: {page}}}) {
  const schema = await api.fetchSchema();

  if (!type) {
    throw new Error("bundle.inputData.type must be provided");
  }
  const typeObject = findTypeObjectByIdOrName(schema.typeObjects, type);
  const idFieldObject = typeObject.idFieldObject;
  const creationDateFieldObject = typeObject.fieldObjects.find((x) => x.rawMeta["fibery/creation-date?"]);

  const objects = await api.queryObjects({
    "q/from": typeObject.name,
    "q/select": getSelectWithSupportedFields(schema.typeObjects, type),
    "q/order-by": creationDateFieldObject
      ? [
        [[creationDateFieldObject.name], "q/desc"],
        [[idFieldObject.name], "q/asc"],
      ]
      : [
        [[idFieldObject.name], "q/asc"],
      ],
    "q/limit": PAGE_LIMIT,
    "q/offset": PAGE_LIMIT * page,
  });

  const objectsWithDocuments = await withRichTextFieldsDocumentsLoaded(objects, typeObject, api);

  return withObjectLink(objectsWithDocuments, typeObject, api).map((object) => {
    return provideZapierId({object, typeObject});
  });
}

module.exports = {performListCreatedOperation};
