const _ = require("lodash");
const {createOrUpdateObject} = require("../../utils");
const {randomUUID} = require("node:crypto");

const FIELD_KEY_PREFIX = "CREATE:";

function performCreateOperation({api, bundle: {inputData}}, generateUUID = randomUUID) {
  const {type} = inputData;
  if (!type) {
    throw new Error("bundle.inputData.type must be provided");
  }
  const rest = _.omit(inputData, "type");
  const rawCreateInputData = _.pickBy(rest, (value, key) => key.indexOf(FIELD_KEY_PREFIX) >= 0);
  const data = _.flow([
    _.toPairs,
    (pairs) => _.map(pairs, ([key, value]) => {
      return [key.replace(FIELD_KEY_PREFIX, ""), value];
    }),
    _.fromPairs,
  ])(rawCreateInputData);

  return createOrUpdateObject({api, type, data}, generateUUID);
}

module.exports = {performCreateOperation, FIELD_KEY_PREFIX};
