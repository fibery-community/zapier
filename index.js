const addBearerHeader = (request, z, bundle) => {
  if (bundle.authData && bundle.authData.access_token) {
    request.headers.Authorization = `Bearer ${bundle.authData.access_token}`;
  }
  return request;
};

module.exports = {
  version: require("./package.json").version,
  platformVersion: require("zapier-platform-core").version,
  authentication: require("./authentication"),
  beforeRequest: [addBearerHeader],
  resources: {
    type: require("./resources/type"),
    object: require("./resources/object"),
  },
  triggers: {
    listByModificationDate: require("./resources/object/listByModificationDate"),
  },
  creates: {
    update: require("./resources/object/update"),
  },
  hydrators: {
    stashFile: require("./resources/file/stashFileFunction"),
  },
};
