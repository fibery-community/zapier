const {typeField, idField} = require("./commons");
const {toManagedFunction} = require("../../utils");
const {performUpdateOperation} = require("./update.operation.perform");
const {makeTypeDependantFieldsRetriever} = require("./create.operation.inputFields");

module.exports = {
  key: "update",
  noun: "Entity",
  display: {
    label: `Update Entity`,
    description: `Updates existing Entity (Task, Document, etc.).`,
  },
  operation: {
    inputFields: [
      typeField,
      idField,
      toManagedFunction(makeTypeDependantFieldsRetriever({fieldFilterFn: (fieldObject) => fieldObject.isReadOnly === false})),
    ],
    sample: {id: "00000000-0000-0000-0000-000000000000"},
    perform: toManagedFunction(performUpdateOperation),
  },
};
