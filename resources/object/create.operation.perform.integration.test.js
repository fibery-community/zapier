const {performCreateOperation} = require("./create.operation.perform");

const makeZ = () => ({
  request: (params) => {
    const request = require("request");
    const options = Object.assign({}, params, {body: JSON.stringify(params.body)});
    return new Promise((resolve, reject) => request[params.method.toLowerCase()](
      options,
      (error, httpResponse, body) => {
        if (error) {
          return reject(error);
        }
        let json = undefined;
        try {
          json = JSON.parse(body);
        } catch (e) {
          console.error("non-json body", body);
        }

        const complete = httpResponse.statusCode === 200 ? resolve : reject;
        const result = Object.assign({json, body, status: httpResponse.statusCode}, httpResponse);
        return complete(result);
      }));
  },
  console,
});

test.skip("failing case", () => {
  jest.setTimeout(10000);

  const promise = require("../../failing-case").creates.uploadFile.operation.perform();
  return expect(promise).resolves.toBe("");
});

const zapierAttachmentUrl = "https://zapier.com/engine/hydrate/2729731/.eJxVkseSo1gQRf-FdWkCb2qHACFAIIQQbmLiBfDwCIQ3FfXvrTaLnm3mzZsZ9-QXUjTDGDZxAgqIfBI4TVAUin8gaZHUEDThM0E-kXAcwzh_Js04IB9InCdxBapkQz5xBucYAnvX2mZ8t8G4vX4OGG9ZtYR9NiCfX8gzGYYw-70BwWiM5VIaZUMYoQmNvpV_23_--4X80vGGXEL1cDEnyJaR6x7NbjPsjj9XMxy0gKKBXy8pkE5yrlmRgsrV0Yy5SREiKT7uOwNkSOqRy4YoV-YMtha7gGVoSDBsxHYVZi7OCWqCMjYvnrPyh6T1Q-I4gktZHIwdjxhN0xNypsa1SsTxJpDW4W0CizrPZbK_E5VonrOzPr-jmCJlcnFyeqyb61K3FmSNtevr0UuPFtwWUpWCGRKzb0UPkUhWNS5Y0-buji3jR_A-u42iFaCDKYc0GaTrI3_JfQeo8OB4YZqa-1XyCMrE0Lm8GcM4Te11wAVohLx0ee0Bh7bFVt3zK-H3oh0oNrY-MkbEFJ0lhWJbKPmoXmqZGU9j8ZLC3WYZca8XFjiueDc560a5BSnfKkM42H1QTMvLe52dNOFK0sbqu8t7Lb-rr05UfsL_8xEYwP4pXxny_fE_XJx5iPoKjLoOZkb3xZTXMElXXDffHLyo21jfAmAQ-Hx58U2Cd4TOWgBtz64_z7W5e22NpdkUi2um0rmmZqMjaMS2Wt2wN7fk5G-Qm1os83FJSW2_FR5JzZqT0Xvj2ERHvIrORAAI2gmCS9oArFtJeI3rgzw9b4p0tuWllENxRTfYeqXYKfZDi2_3aLtRlXF1yaFl9nPKEGG9irQyCOSUEmpkUKzBiKfsyqpz7vrRjexQrWOuak_1yaqT5HMMDxPnrUR4AOwaqeiZNy5HJxHxYuNFdTmc5gDHe43FFTD4yqkfFugzVzzDfFM-0DKxuihLzWDXAQaJaDmNYs81V8aSpK45wzK1rci94CasWYXos_X-grMjN8faI-u4LsyeEjh24L3qOQ5yeS8Pi9D6dpn_jQtGMQUxjk7DmOJCCCmSY8OYoxKYoDHF_mb53_f3D_WJV9k:1elD6p:OmB_2GLr1s5pRBBEiPRJtSK4tXA/";

test.skip("integration", () => {
  jest.setTimeout(10000);

  const bundle = {
    authData: {host: "http://alpha.fibery.io"},
    //authData: {host: "http://localhost:9000"},
    inputData: {
      type: "kanban/feature",
      "kanban/name": "local test with attachments",
      "fibery/files": [
        zapierAttachmentUrl,
      ],
    },
    environment: {},
  };
  const api = require("../../api")(makeZ(), bundle);
  const createdFiles = performCreateOperation({api, bundle}).then((x) => x["fibery/files"]);
  return expect(createdFiles).resolves.toHaveLength(1);
});
