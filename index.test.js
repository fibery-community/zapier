const _ = require("lodash");

test("exports everything", () => {
  expect(_.omit(require("./index"), ["version"])).toMatchSnapshot();
});
