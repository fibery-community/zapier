const {toManagedFunction} = require("../../utils");

module.exports = {
  display: {
    label: `New Type`,
    description: `Triggers when a new type is created.`,
    important: false,
  },
  operation: {
    perform: toManagedFunction(function listAllTypes({api}) {
      return api.fetchSchema().then((schema) => {
        return schema.typeObjects.filter((typeObject) => typeObject.isDomain)
          .map((typeObject) => ({
            id: typeObject.id,
            title: typeObject.name,
          }));
      });
    }),
    sample: {id: "00000000-0000-0000-0000-000000000000", title: "People/User"},
  },
};
