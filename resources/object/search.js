const {typeField} = require("./commons");
const {toManagedFunction} = require("../../utils");
const {performSearchOperation, FIELD_KEY_PREFIX} = require("./search.operation.perform");
const {makeTypeDependantFieldsRetriever} = require("./create.operation.inputFields");

module.exports = {
  display: {
    label: `Find Entity`,
    description: `Finds an Entity with matching field values.`,
  },
  operation: {
    inputFields: [
      typeField,
      toManagedFunction(makeTypeDependantFieldsRetriever({
        fieldKeyPrefix: FIELD_KEY_PREFIX,
        fieldFilterFn: () => true,
      })),
    ],
    sample: {id: "00000000-0000-0000-0000-000000000000"},
    perform: toManagedFunction(performSearchOperation),
  },
};
