const {performListByModificationDateOperation} = require("./listByModificationDate.operation.perform");
const {makeApiMock} = require("./mock");
const schema = require("./software-dev.schema");

describe(`performListByModificationDateOperation`, () => {
  const setupStubs = () => {
    const calls = [
      {
        call: (api) => api.fetchSchema(),
        body: () => Promise.resolve(schema),
      },
      {
        call: (api) => api.queryObjects({
          "q/from": "software-development/bug",
          "q/select":
            {
              "Steps To Reproduce": {
                "Fibery Id": ['software-development/steps-to-reproduce', 'fibery/id'],
                "Secret": ['software-development/steps-to-reproduce', 'Collaboration~Documents/secret']
              },
              "User Story":
                {
                  "Fibery Id": ["software-development/user-story", "fibery/id"],
                  "Creation Date": ["software-development/user-story", "fibery/creation-date"],
                  "Public Id": ["software-development/user-story", "fibery/public-id"],
                  Effort:
                    [
                      "software-development/user-story",
                      "software-development/effort",
                    ],
                  Name:
                    [
                      "software-development/user-story",
                      "software-development/name",
                    ],
                },
              Assignees:
                {
                  "q/from": ["assignments/assignees"],
                  "q/limit": "q/no-limit",
                  "q/select":
                    {
                      Name: "user/name",
                      "Fibery Id": "fibery/id",
                      "Creation Date": "fibery/creation-date",
                      "Public Id": "fibery/public-id",
                      "First Login?": "fibery/first-login?",
                      Email: "user/email",
                    },
                },
              "Fibery Id": "fibery/id",
              "Creation Date": "fibery/creation-date",
              "Modification Date": "fibery/modification-date",
              "Public Id": "fibery/public-id",
              Severity:
                {
                  "Fibery Id": ["software-development/severity", "fibery/id"],
                  "Public Id": ["software-development/severity", "fibery/public-id"],
                  Name: ["software-development/severity", "enum/name"],
                  Value:
                    [
                      "software-development/severity",
                      "software-development/Value",
                    ],
                },
              State:
                {
                  "Fibery Id": ["workflow/state", "fibery/id"],
                  "Public Id": ["workflow/state", "fibery/public-id"],
                  Name: ["workflow/state", "enum/name"],
                  Final: ["workflow/state", "workflow/Final"],
                },
              Effort: "software-development/effort",
              Priority:
                {
                  "Fibery Id": ["software-development/priority", "fibery/id"],
                  "Public Id": ["software-development/priority", "fibery/public-id"],
                  Name: ["software-development/priority", "enum/name"],
                  Value:
                    [
                      "software-development/priority",
                      "software-development/Value",
                    ],
                },
              Name: "software-development/name",
              Release:
                {
                  "Fibery Id": ["software-development/release", "fibery/id"],
                  "Creation Date": ["software-development/release", "fibery/creation-date"],
                  "Public Id": ["software-development/release", "fibery/public-id"],
                  Name: ["software-development/release", "software-development/name"],
                  "Assigned Effort":
                    [
                      "software-development/release",
                      "software-development/assigned-effort",
                    ],
                  "Completed Effort":
                    [
                      "software-development/release",
                      "software-development/completed-effort",
                    ],
                },
              Files:
                {
                  "q/from": ["fibery/files"],
                  "q/limit": "q/no-limit",
                  "q/select":
                    {
                      "Fibery Id": "fibery/id",
                      Name: "fibery/name",
                      "Content Type": "fibery/content-type",
                      Secret: "fibery/secret",
                    },
                },
              Pain: "software-development/Pain",
            },
          "q/order-by": [[["fibery/modification-date"], "q/desc"], [["fibery/id"], "q/asc"]],
          "q/limit": 100,
          "q/offset": 0,
        }),
        body: () => Promise.resolve([
          {
            "Fibery Id": "fibery/id",
            "Public Id": 1,
            "Modification Date": "2018-01-01T12:00:00.000Z",
            "Steps To Reproduce": {
              "Secret": "324d3bb1-740d-11e9-8f76-85b9cee696f7",
              "Fibery Id": "324d3bb0-740d-11e9-8f76-85b9cee696f7",
            },
            Files: [
              {
                "Fibery Id": "fibery/id",
                Name: "fibery/name",
                "Content Type": "fibery/content-type",
                Secret: "fibery/secret",
              }
            ]
          },
        ]),
      },
      {
        call: (api) => api.queryCollaborationDocuments(["324d3bb1-740d-11e9-8f76-85b9cee696f7"]),
        body: () => Promise.resolve([
          {
            "secret": "324d3bb1-740d-11e9-8f76-85b9cee696f7",
            "content": "some text",
          },
        ]),
      },
      {
        call: (api) => api.getFiberyHost(),
        body: () => "test.fibery.io",
      },
      {
        call: (api) => api.stashFile("fibery/secret"),
        body: () => "stashed file",
      },
      {
        call: (api) => api.stashFile("fibery/secret"),
        body: () => "stashed file",
      },
    ];
    return {calls};
  };

  test("using type id", async () => {
    const {calls} = setupStubs();

    const result = await performListByModificationDateOperation({
      api: makeApiMock(calls).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "2703295f-482a-4c49-944f-34961fc6503b",
        },
        meta: {page: 0},
      },
    });
    return expect(result).toEqual([
      {
        "Entity Link": "https://test.fibery.io/software-development/bug/1",
        "id": "fibery/id@2018-01-01T12:00:00.000Z",
        "Fibery Id": "fibery/id",
        "Public Id": 1,
        "Modification Date": "2018-01-01T12:00:00.000Z",
        "Steps To Reproduce": "some text",
        "Files": [
          {
            "Content Type": "fibery/content-type",
            "Fibery Id": "fibery/id",
            "File": "stashed file",
            "Name": "fibery/name",
            "Secret": "fibery/secret"
          }
        ],
      },
    ]);
  });

  test("using type name", async () => {
    const {calls} = setupStubs();

    const result = await performListByModificationDateOperation({
      api: makeApiMock(calls).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "software-development/bug",
        },
        meta: {page: 0},
      },
    });
    return expect(result).toEqual([
      {
        "Entity Link": "https://test.fibery.io/software-development/bug/1",
        "id": "fibery/id@2018-01-01T12:00:00.000Z",
        "Fibery Id": "fibery/id",
        "Public Id": 1,
        "Modification Date": "2018-01-01T12:00:00.000Z",
        "Steps To Reproduce": "some text",
        "Files": [
          {
            "Content Type": "fibery/content-type",
            "Fibery Id": "fibery/id",
            "File": "stashed file",
            "Name": "fibery/name",
            "Secret": "fibery/secret"
          }
        ],
      },
    ]);
  });
});

