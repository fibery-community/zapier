module.exports = {
  key: "entity",
  noun: "Entity",
  list: require("./listCreated"),
  create: require("./create"),
  search: require("./search"),
};
