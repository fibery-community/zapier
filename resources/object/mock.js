const _ = require("lodash");
const util = require("util");
const catchCall = (call) => {
  let result = undefined;
  const trap = new Proxy({}, {
    get(target, methodName) {
      return function(...args) {
        if (result !== undefined) {
          throw new Error("you must not make multiple calls");
        }
        result = {methodName, args};
      };
    },
  });
  call(trap);
  if (result === undefined) {
    throw new Error("call method must implement a single call");
  }
  return result;
};

const printCall = ({methodName, args}) => `api.${methodName}(${util.inspect(args, {
  depth: null,
  maxArrayLength: null,
}).replace(/\s?]$/, "").replace(/^\[\s*/, "")})`;

const makeApiMock = (calls) => {
  let remainingCalls = [];
  const proxy = new Proxy({}, {
    get(target, methodName) {
      if (remainingCalls.length === 0) {
        return function(...args) {
          throw new Error(`call was not expected (expectations are empty):\n${printCall({
            methodName,
            args,
          })}\n`);
        };
      }
      const expectedCall = _.pick(remainingCalls[0], ["methodName", "args"]);
      const body = remainingCalls[0].body;
      remainingCalls = remainingCalls.slice(1);

      return function(...args) {
        const actualCall = {methodName, args};
        if (!_.isEqual(expectedCall, actualCall)) {
          const printedActualCall = printCall(actualCall);
          const printedExpectedCall = printCall(expectedCall);
          expect(`(api) => ${printedActualCall}`).toBe(`(api) => ${printedExpectedCall}`);
        }
        return body(...args);
      };
    },
  });
  const apiMock = {
    proxy,
    getRemainingCalls: () => remainingCalls,
    expectMore: (calls) => remainingCalls = remainingCalls.concat(calls.map(({call, body}) => {
      if (!_.isFunction(call)) {
        throw new Error("call must be a function");
      }
      if (!_.isFunction(body)) {
        throw new Error("body must be a function");
      }
      const {methodName, args} = catchCall(call);
      return {methodName, args, body};
    })),
  };
  apiMock.expectMore(calls);
  return apiMock;
};

module.exports = {makeApiMock};
