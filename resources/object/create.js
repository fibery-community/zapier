const {typeField} = require("./commons");
const {toManagedFunction} = require("../../utils");
const {performCreateOperation, FIELD_KEY_PREFIX} = require("./create.operation.perform");
const {makeTypeDependantFieldsRetriever} = require("./create.operation.inputFields");

module.exports = {
  display: {
    label: `New Entity`,
    description: `Creates a new Entity (Task, Document, etc.).`,
  },
  operation: {
    inputFields: [
      typeField,
      toManagedFunction(makeTypeDependantFieldsRetriever({
        fieldKeyPrefix: FIELD_KEY_PREFIX,
        fieldFilterFn: (fieldObject) => fieldObject.isReadOnly === false,
      })),
    ],
    sample: {id: "00000000-0000-0000-0000-000000000000"},
    perform: toManagedFunction(performCreateOperation),
  },
};
