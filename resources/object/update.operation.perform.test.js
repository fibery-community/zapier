const {performUpdateOperation} = require("./update.operation.perform");
const {makeApiMock} = require("./mock");
const schema = require("./software-dev.schema");

describe("performUpdateOperation", () => {
  const setupStubs = () => {
    const calls = [
      {
        call: (api) => api.fetchSchema(),
        body: () => Promise.resolve(schema),
      },
      {
        call: (api) => api.update({
          type: "software-development/bug",
          entity: {"fibery/id": "test-007"},
          query: {
            "q/from": "software-development/bug",
            "q/select":
              {
                "fibery/id": "fibery/id",
                "Steps To Reproduce": {
                  "Fibery Id": ["software-development/steps-to-reproduce", "fibery/id"],
                  "Secret": ["software-development/steps-to-reproduce", "Collaboration~Documents/secret"]
                },
              },
            "q/where": ["=", ["fibery/id"], "$id"],
            "q/limit": 1,
          },
          params: {$id: "test-007"},
        }),
        body: () => Promise.resolve([{
          "fibery/id": "test-007",
          "Steps To Reproduce": {
            "Secret": "324d3bb1-740d-11e9-8f76-85b9cee696f7",
            "Fibery Id": "324d3bb0-740d-11e9-8f76-85b9cee696f7",
          },
        }]),
      },
      {
        call: (api) => api.updateCollaborationDocuments([{
          secret: "324d3bb1-740d-11e9-8f76-85b9cee696f7",
          content: "1. develop without bugs\n2. profit!"
        }]),
        body: () => null,
      },
      {
        call: (api) => api.addCollectionItems("software-development/bug", "test-007", "software-development/user-story", ["f"]),
        body: () => null,
      },
      {
        call: (api) => api.addCollectionItems("software-development/bug", "test-007", "assignments/assignees", ["a", "b"]),
        body: () => null,
      },
      {
        call: (api) => api.uploadFile("https://example.com/engine/hydrate/id/xxx"),
        body: () => "xxx",
      },
      {
        call: (api) => api.uploadFile("https://example.com/engine/hydrate/id/yyy"),
        body: () => "yyy",
      },
      {
        call: (api) => api.addCollectionItems("software-development/bug", "test-007", "fibery/files", ["xxx", "yyy"]),
        body: () => null,
      },
      {
        call: (api) => api.queryObjects({
            "q/from": "software-development/bug",
            "q/select":
              {
                "Steps To Reproduce": {
                  "Fibery Id": ['software-development/steps-to-reproduce', 'fibery/id'],
                  "Secret": ['software-development/steps-to-reproduce', 'Collaboration~Documents/secret']
                },
                "User Story":
                  {
                    "Fibery Id": ["software-development/user-story", "fibery/id"],
                    "Creation Date": ["software-development/user-story", "fibery/creation-date"],
                    "Public Id": ["software-development/user-story", "fibery/public-id"],
                    Effort:
                      [
                        "software-development/user-story",
                        "software-development/effort",
                      ],
                    Name:
                      [
                        "software-development/user-story",
                        "software-development/name",
                      ],
                  },
                Assignees:
                  {
                    "q/from": ["assignments/assignees"],
                    "q/limit": "q/no-limit",
                    "q/select":
                      {
                        Name: "user/name",
                        "Fibery Id": "fibery/id",
                        "Creation Date": "fibery/creation-date",
                        "Public Id": "fibery/public-id",
                        "First Login?": "fibery/first-login?",
                        Email: "user/email",
                      },
                  },
                "Fibery Id": "fibery/id",
                "Creation Date": "fibery/creation-date",
                "Modification Date": "fibery/modification-date",
                "Public Id": "fibery/public-id",
                Severity:
                  {
                    "Fibery Id": ["software-development/severity", "fibery/id"],
                    "Public Id": ["software-development/severity", "fibery/public-id"],
                    Name: ["software-development/severity", "enum/name"],
                    Value:
                      [
                        "software-development/severity",
                        "software-development/Value",
                      ],
                  },
                State:
                  {
                    "Fibery Id": ["workflow/state", "fibery/id"],
                    "Public Id": ["workflow/state", "fibery/public-id"],
                    Name: ["workflow/state", "enum/name"],
                    Final: ["workflow/state", "workflow/Final"],
                  },
                Effort: "software-development/effort",
                Priority:
                  {
                    "Fibery Id": ["software-development/priority", "fibery/id"],
                    "Public Id": ["software-development/priority", "fibery/public-id"],
                    Name: ["software-development/priority", "enum/name"],
                    Value:
                      [
                        "software-development/priority",
                        "software-development/Value",
                      ],
                  },
                Name: "software-development/name",
                Release:
                  {
                    "Fibery Id": ["software-development/release", "fibery/id"],
                    "Creation Date": ["software-development/release", "fibery/creation-date"],
                    "Public Id": ["software-development/release", "fibery/public-id"],
                    Name: ["software-development/release", "software-development/name"],
                    "Assigned Effort":
                      [
                        "software-development/release",
                        "software-development/assigned-effort",
                      ],
                    "Completed Effort":
                      [
                        "software-development/release",
                        "software-development/completed-effort",
                      ],
                  },
                Files:
                  {
                    "q/from": ["fibery/files"],
                    "q/limit": "q/no-limit",
                    "q/select":
                      {
                        "Fibery Id": "fibery/id",
                        Name: "fibery/name",
                        "Content Type": "fibery/content-type",
                        Secret: "fibery/secret",
                      },
                  },
                Pain: "software-development/Pain",
              },
            "q/where": ["=", ["fibery/id"], "$id"],
            "q/limit": 1,
          },
          {"$id": "test-007"}),
        body: () => Promise.resolve([
          {
            "id": "fibery/id",
            "other/param": "other/param",
            "Steps To Reproduce": {
              "Secret": "324d3bb1-740d-11e9-8f76-85b9cee696f7",
              "Fibery Id": "324d3bb0-740d-11e9-8f76-85b9cee696f7",
            },
          },
        ]),
      },
    ];
    return {calls};
  };

  test("via type id", async () => {
    const {calls} = setupStubs();
    const result = await performUpdateOperation({
      api: makeApiMock(calls).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "2703295f-482a-4c49-944f-34961fc6503b",
          "id": "test-007",
          "16ee3b91-b584-4662-97fd-2b9d134fc06f": "1. develop without bugs\n2. profit!",
          "d78f224c-a85a-4335-bc6c-c7cd5468adf0": "f",
          "d3213627-55b3-4a66-8a8d-0280d9bcd133": ["a", "b"],
          "8daaf0aa-78b6-4d5a-8385-e61ba1e53cff": [
            "https://example.com/engine/hydrate/id/xxx",
            "https://example.com/engine/hydrate/id/yyy",
          ],
        },
      },
    });
    return expect(result).toEqual({
      "id": "fibery/id",
      "other/param": "other/param",
      "Steps To Reproduce": "1. develop without bugs\n2. profit!",
    });
  });

  test("via type name", async () => {
    const {calls} = setupStubs();
    const result = await performUpdateOperation({
      api: makeApiMock(calls).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "software-development/bug",
          "id": "test-007",
          "16ee3b91-b584-4662-97fd-2b9d134fc06f": "1. develop without bugs\n2. profit!",
          "d78f224c-a85a-4335-bc6c-c7cd5468adf0": "f",
          "d3213627-55b3-4a66-8a8d-0280d9bcd133": ["a", "b"],
          "8daaf0aa-78b6-4d5a-8385-e61ba1e53cff": [
            "https://example.com/engine/hydrate/id/xxx",
            "https://example.com/engine/hydrate/id/yyy",
          ],
        },
      },
    });
    return expect(result).toEqual({
      "id": "fibery/id",
      "other/param": "other/param",
      "Steps To Reproduce": "1. develop without bugs\n2. profit!",
    });
  });
});

test("performUpdateOperation with date range fields", async () => {
  const calls = [
    {
      call: (api) => api.fetchSchema(),
      body: () => Promise.resolve(schema),
    },
    {
      call: (api) => api.update({
        type: "software-development/user-story",
        entity: {
          "fibery/id": "test-007",
          "software-development/planned-dates": {start: "2015-03-05", end: "2016-03-05"},
        },
        query: {
          "q/from": "software-development/user-story",
          "q/select": {"fibery/id": "fibery/id"},
          "q/where": ["=", ["fibery/id"], "$id"],
          "q/limit": 1,
        },
        params: {'$id': 'test-007'}
      }),
      body: () => Promise.resolve([{"fibery/id": "test-007"}]),
    },
    {
      call: (api) => api.queryObjects({
          "q/from": "software-development/user-story",
          "q/select":
            {
              "Planned Dates": "software-development/planned-dates",
              Assignees:
                {
                  "q/from": ["assignments/assignees"],
                  "q/limit": "q/no-limit",
                  "q/select":
                    {
                      Name: "user/name",
                      "Fibery Id": "fibery/id",
                      "Creation Date": "fibery/creation-date",
                      "Public Id": "fibery/public-id",
                      "First Login?": "fibery/first-login?",
                      Email: "user/email",
                    },
                },
              "Fibery Id": "fibery/id",
              "Creation Date": "fibery/creation-date",
              "Public Id": "fibery/public-id",
              Bugs:
                {
                  "q/from": ["software-development/Bugs"],
                  "q/limit": "q/no-limit",
                  "q/select":
                    {
                      "Fibery Id": "fibery/id",
                      "Creation Date": "fibery/creation-date",
                      "Modification Date": "fibery/modification-date",
                      "Public Id": "fibery/public-id",
                      Effort: "software-development/effort",
                      Name: "software-development/name",
                      Pain: "software-development/Pain",
                    },
                },
              State:
                {
                  Final: ["workflow/state", "workflow/Final"],
                  "Fibery Id": ["workflow/state", "fibery/id"],
                  "Public Id": ["workflow/state", "fibery/public-id"],
                  Name: ["workflow/state", "enum/name"],
                },
              Effort: "software-development/effort",
              Name: "software-development/name",
              Release:
                {
                  "Fibery Id": ["software-development/release", "fibery/id"],
                  "Creation Date": ["software-development/release", "fibery/creation-date"],
                  "Public Id": ["software-development/release", "fibery/public-id"],
                  Name: ["software-development/release", "software-development/name"],
                  "Assigned Effort":
                    [
                      "software-development/release",
                      "software-development/assigned-effort",
                    ],
                  "Completed Effort":
                    [
                      "software-development/release",
                      "software-development/completed-effort",
                    ],
                },
              Files:
                {
                  "q/from": ["fibery/files"],
                  "q/limit": "q/no-limit",
                  "q/select":
                    {
                      "Fibery Id": "fibery/id",
                      Name: "fibery/name",
                      "Content Type": "fibery/content-type",
                      Secret: "fibery/secret",
                    },
                },
            },
          "q/where": ["=", ["fibery/id"], "$id"],
          "q/limit": 1,
        },
        {"$id": "test-007"}),
      body: () => Promise.resolve([
        {
          "id": "fibery/id",
          "other/param": "other/param",
        },
      ]),
    },
  ];
  const result = await performUpdateOperation({
    api: makeApiMock(calls).proxy,
    bundle: {
      authData: {host: "host"},
      inputData: {
        type: "software-development/user-story",
        "id": "test-007",
        "9590c359-de96-41da-8372-c068e757c6bf_start": "2015-03-05T12:07:40+03:00",
        "9590c359-de96-41da-8372-c068e757c6bf_end": "2016-03-05T12:07:40+03:00",
      },
    },
  });
  return expect(result).toEqual({"id": "fibery/id", "other/param": "other/param"});
});
