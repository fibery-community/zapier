const typeField = {
  key: "type",
  type: "string",
  label: "Type",
  required: true,
  dynamic: "typeList.id.title",
  altersDynamicFields: true,
};

const idField = {
  key: "id",
  type: "string",
  label: "Fibery Id",
  required: true,
  dynamic: "entityList.id.title",
  altersDynamicFields: false,
};

module.exports = {typeField, idField};
