const {typeField} = require("./commons");
const {toManagedFunction} = require("../../utils");
const {performListCreatedOperation} = require("./listCreated.operation.perform");

// lists latest objects, interpreted as polling trigger
module.exports = {
  display: {
    label: `New Entity`,
    description: `Triggers when a new Entity (Task, Document, etc.) is created.`,
    important: true,
  },
  operation: {
    inputFields: [
      typeField,
    ],
    canPaginate: true,
    perform: toManagedFunction(performListCreatedOperation),
    sample: {id: "00000000-0000-0000-0000-000000000000"},
  },
};
