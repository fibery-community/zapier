function stashFileFunction(z, bundle) {
  const fileUrl = bundle.inputData.fileUrl;
  // use standard auth to request the file
  const filePromise = z.request({
    url: bundle.inputData.fileUrl,
    raw: true,
  });

  z.console.log(`stash file ${fileUrl}`);
  return z.stashFile(filePromise);
}

module.exports = stashFileFunction;
