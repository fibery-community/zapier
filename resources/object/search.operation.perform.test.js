const {performSearchOperation} = require("./search.operation.perform");
const {makeApiMock} = require("./mock");
const schema = require("./software-dev.schema");

describe("performSearchOperation", () => {
  const setupStubs = ({fieldKeys}) => {
    const calls = [
      {
        call: (api) => api.fetchSchema(),
        body: () => Promise.resolve(schema),
      },
      {
        call: (api) => api.queryObjects({
            "q/from": "software-development/bug",
            "q/limit": "q/no-limit",
            "q/select":
              {
                "Steps To Reproduce": {
                  "Fibery Id": ["software-development/steps-to-reproduce", "fibery/id"],
                  "Secret": ["software-development/steps-to-reproduce", "Collaboration~Documents/secret"],
                },
                "User Story":
                  {
                    "Fibery Id": ["software-development/user-story", "fibery/id"],
                    "Creation Date": ["software-development/user-story", "fibery/creation-date"],
                    "Public Id": ["software-development/user-story", "fibery/public-id"],
                    Effort:
                      [
                        "software-development/user-story",
                        "software-development/effort",
                      ],
                    Name:
                      [
                        "software-development/user-story",
                        "software-development/name",
                      ],
                  },
                Assignees:
                  {
                    "q/from": ["assignments/assignees"],
                    "q/limit": "q/no-limit",
                    "q/select":
                      {
                        Name: "user/name",
                        "Fibery Id": "fibery/id",
                        "Creation Date": "fibery/creation-date",
                        "Public Id": "fibery/public-id",
                        "First Login?": "fibery/first-login?",
                        Email: "user/email",
                      },
                  },
                "Fibery Id": "fibery/id",
                "Creation Date": "fibery/creation-date",
                "Modification Date": "fibery/modification-date",
                "Public Id": "fibery/public-id",
                Severity:
                  {
                    "Fibery Id": ["software-development/severity", "fibery/id"],
                    "Public Id": ["software-development/severity", "fibery/public-id"],
                    Name: ["software-development/severity", "enum/name"],
                    Value:
                      [
                        "software-development/severity",
                        "software-development/Value",
                      ],
                  },
                State:
                  {
                    "Fibery Id": ["workflow/state", "fibery/id"],
                    "Public Id": ["workflow/state", "fibery/public-id"],
                    Name: ["workflow/state", "enum/name"],
                    Final: ["workflow/state", "workflow/Final"],
                  },
                Effort: "software-development/effort",
                Priority:
                  {
                    "Fibery Id": ["software-development/priority", "fibery/id"],
                    "Public Id": ["software-development/priority", "fibery/public-id"],
                    Name: ["software-development/priority", "enum/name"],
                    Value:
                      [
                        "software-development/priority",
                        "software-development/Value",
                      ],
                  },
                Name: "software-development/name",
                Release:
                  {
                    "Fibery Id": ["software-development/release", "fibery/id"],
                    "Creation Date": ["software-development/release", "fibery/creation-date"],
                    "Public Id": ["software-development/release", "fibery/public-id"],
                    Name: ["software-development/release", "software-development/name"],
                    "Assigned Effort":
                      [
                        "software-development/release",
                        "software-development/assigned-effort",
                      ],
                    "Completed Effort":
                      [
                        "software-development/release",
                        "software-development/completed-effort",
                      ],
                  },
                Files:
                  {
                    "q/from": ["fibery/files"],
                    "q/limit": "q/no-limit",
                    "q/select":
                      {
                        "Fibery Id": "fibery/id",
                        Name: "fibery/name",
                        "Content Type": "fibery/content-type",
                        Secret: "fibery/secret",
                      },
                  },
                Pain: "software-development/Pain",
              },
            "q/where":
              [
                "and",
                [
                  "=",
                  ["software-development/name"],
                  fieldKeys[0],
                ],
                [
                  "=",
                  ["software-development/user-story", "fibery/id"],
                  fieldKeys[1],
                ],
              ],
          },
          {
            [fieldKeys[0]]: "valueOf(software-development/name)",
            [fieldKeys[1]]: "idOf(software-development/user-story)",
          }),
        body: () => Promise.resolve([
          {
            "Fibery Id": "fibery/id",
            "Public Id": 1,
            "Files": [
              {
                "Fibery Id": "file-id",
                Name: "file-name",
                "Content Type": "file-content-type",
                Secret: "file-secret",
              }
            ]}
        ]),
      },
      {
        call: (api) => api.getFiberyHost(),
        body: () => "test.fibery.io",
      },
      {
        call: (api) => api.stashFile("file-secret"),
        body: () => "stashed file",
      },
      {
        call: (api) => api.stashFile("file-secret"),
        body: () => "stashed file",
      },
    ];
    return {calls};
  };

  test("via type id", async () => {
    const {calls} = setupStubs({
      fieldKeys: ["$c475509c-adf6-4ccc-a5b4-d0bafdf07475", "$d78f224c-a85a-4335-bc6c-c7cd5468adf0"],
    });
    const result = await performSearchOperation({
      api: makeApiMock(calls).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "2703295f-482a-4c49-944f-34961fc6503b",
          "SEARCH:c475509c-adf6-4ccc-a5b4-d0bafdf07475": "valueOf(software-development/name)",
          "SEARCH:d78f224c-a85a-4335-bc6c-c7cd5468adf0": "idOf(software-development/user-story)",
        },
      },
    });
    return expect(result).toEqual([
      {
        "Entity Link": "https://test.fibery.io/software-development/bug/1",
        "Fibery Id": "fibery/id",
        "id": "fibery/id",
        "Public Id": 1,
        "Files": [
          {
            "Content Type": "file-content-type",
            "Fibery Id": "file-id",
            "Name": "file-name",
            "Secret": "file-secret",
            "File": "stashed file"
          }
        ]
      },
    ]);
  });

  test("via type name", async () => {
    const {calls} = setupStubs({
      fieldKeys: ["$c475509c-adf6-4ccc-a5b4-d0bafdf07475", "$d78f224c-a85a-4335-bc6c-c7cd5468adf0"],
    });
    const result = await performSearchOperation({
      api: makeApiMock(calls).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "software-development/bug",
          "SEARCH:c475509c-adf6-4ccc-a5b4-d0bafdf07475": "valueOf(software-development/name)",
          "SEARCH:d78f224c-a85a-4335-bc6c-c7cd5468adf0": "idOf(software-development/user-story)",
        },
      },
    });
    return expect(result).toEqual([
      {
        "Entity Link": "https://test.fibery.io/software-development/bug/1",
        "Fibery Id": "fibery/id",
        "id": "fibery/id",
        "Public Id": 1,
        "Files": [
          {
            "Content Type": "file-content-type",
            "Fibery Id": "file-id",
            "File": "stashed file",
            "Name": "file-name",
            "Secret": "file-secret"
          }
        ],
      },
    ]);
  });

  test("via field names", async () => {
    const {calls} = setupStubs({fieldKeys: ["$software-development$name", "$software-development$user-story"]});
    const result = await performSearchOperation({
      api: makeApiMock(calls).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "software-development/bug",
          "SEARCH:software-development/name": "valueOf(software-development/name)",
          "SEARCH:software-development/user-story": "idOf(software-development/user-story)",
        },
      },
    });
    return expect(result).toEqual([
      {
        "Entity Link": "https://test.fibery.io/software-development/bug/1",
        "Fibery Id": "fibery/id", "id": "fibery/id",
        "Public Id": 1,
        "Files": [
          {
            "Content Type": "file-content-type",
            "Fibery Id": "file-id",
            "File": "stashed file",
            "Name": "file-name",
            "Secret": "file-secret"
          }
        ],
      },
    ]);
  });
});

