const {typeField} = require("./commons");
const {toManagedFunction} = require("../../utils");
const {performListByModificationDateOperation} = require("./listByModificationDate.operation.perform");

module.exports = {
  key: "listByModificationDate",
  noun: "Entity",
  display: {
    label: `Entity Updated (Or Created)`,
    description: `Triggers when a new Entity (Task, Document, etc.) is created or updated.`,
    important: true,
  },
  operation: {
    type: "polling",
    inputFields: [
      typeField,
    ],
    sample: {id: "00000000-0000-0000-0000-000000000000@"},
    canPaginate: true,
    perform: toManagedFunction(performListByModificationDateOperation),
  },
};
