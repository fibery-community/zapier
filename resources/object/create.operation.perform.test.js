const {performCreateOperation} = require("./create.operation.perform");
const {makeApiMock} = require("./mock");
const schema = require("./software-dev.schema");

describe("performCreateOperation", () => {
  const setupCalls = () => [
    {
      call: (api) => api.fetchSchema(),
      body: () => Promise.resolve(schema),
    },
    {
      call: (api) => api.create({
        type: "software-development/bug",
        entity: {
          "fibery/id": "1",
        },
        query: {
          "q/from": "software-development/bug",
          "q/select":
            {
              "fibery/id": "fibery/id",
              "Steps To Reproduce": {
                "Fibery Id": ["software-development/steps-to-reproduce", "fibery/id"],
                "Secret": ["software-development/steps-to-reproduce", "Collaboration~Documents/secret"]
              },
            },
          "q/where": ["=", ["fibery/id"], "$id"],
          "q/limit": 1,
        },
        params: {$id: "1"},
      }),
      body: () => Promise.resolve([{
        "fibery/id": "1",
        "Steps To Reproduce": {
          "Secret": "324d3bb1-740d-11e9-8f76-85b9cee696f7",
          "Fibery Id": "324d3bb0-740d-11e9-8f76-85b9cee696f7",
        },
      }]),
    },
    {
      call: (api) => api.updateCollaborationDocuments([{
        secret: "324d3bb1-740d-11e9-8f76-85b9cee696f7",
        content: "1. develop without bugs\n2. profit!"
      }]),
      body: () => null,
    },
    {
      call: (api) => api.addCollectionItems("software-development/bug", "1", "software-development/user-story", ["u"]),
      body: () => null,
    },
    {
      call: (api) => api.addCollectionItems("software-development/bug", "1", "assignments/assignees", ["a", "b"]),
      body: () => null,
    },
    {
      call: (api) => api.uploadFile("https://example.com/engine/hydrate/id/xxx"),
      body: () => "xxx",
    },
    {
      call: (api) => api.uploadFile("https://example.com/engine/hydrate/id/yyy"),
      body: () => "yyy",
    },
    {
      call: (api) => api.addCollectionItems("software-development/bug", "1", "fibery/files", ["xxx", "yyy"]),
      body: () => null,
    },
    {
      call: (api) => api.queryObjects({
          "q/from": "software-development/bug",
          "q/select":
            {
              "Steps To Reproduce": {
                "Fibery Id": ["software-development/steps-to-reproduce", "fibery/id"],
                "Secret": ["software-development/steps-to-reproduce", "Collaboration~Documents/secret"]
              },
              "User Story":
                {
                  "Fibery Id": ["software-development/user-story", "fibery/id"],
                  "Creation Date": ["software-development/user-story", "fibery/creation-date"],
                  "Public Id": ["software-development/user-story", "fibery/public-id"],
                  Effort:
                    [
                      "software-development/user-story",
                      "software-development/effort",
                    ],
                  Name:
                    [
                      "software-development/user-story",
                      "software-development/name",
                    ],
                },
              Assignees:
                {
                  "q/from": ["assignments/assignees"],
                  "q/limit": "q/no-limit",
                  "q/select":
                    {
                      Name: "user/name",
                      "Fibery Id": "fibery/id",
                      "Public Id": "fibery/public-id",
                      "Creation Date": "fibery/creation-date",
                      "First Login?": "fibery/first-login?",
                      Email: "user/email",
                    },
                },
              "Fibery Id": "fibery/id",
              "Creation Date": "fibery/creation-date",
              "Modification Date": "fibery/modification-date",
              "Public Id": "fibery/public-id",
              Severity:
                {
                  "Fibery Id": ["software-development/severity", "fibery/id"],
                  "Public Id": ["software-development/severity", "fibery/public-id"],
                  Name: ["software-development/severity", "enum/name"],
                  Value:
                    [
                      "software-development/severity",
                      "software-development/Value",
                    ],
                },
              State:
                {
                  "Fibery Id": ["workflow/state", "fibery/id"],
                  "Public Id": ["workflow/state", "fibery/public-id"],
                  Name: ["workflow/state", "enum/name"],
                  Final: ["workflow/state", "workflow/Final"],
                },
              Effort: "software-development/effort",
              Priority:
                {
                  "Fibery Id": ["software-development/priority", "fibery/id"],
                  "Public Id": ["software-development/priority", "fibery/public-id"],
                  Name: ["software-development/priority", "enum/name"],
                  Value:
                    [
                      "software-development/priority",
                      "software-development/Value",
                    ],
                },
              Name: "software-development/name",
              Release:
                {
                  "Fibery Id": ["software-development/release", "fibery/id"],
                  "Creation Date": ["software-development/release", "fibery/creation-date"],
                  "Public Id": ["software-development/release", "fibery/public-id"],
                  Name: ["software-development/release", "software-development/name"],
                  "Assigned Effort":
                    [
                      "software-development/release",
                      "software-development/assigned-effort",
                    ],
                  "Completed Effort":
                    [
                      "software-development/release",
                      "software-development/completed-effort",
                    ],
                },
              Files:
                {
                  "q/from": ["fibery/files"],
                  "q/limit": "q/no-limit",
                  "q/select":
                    {
                      "Fibery Id": "fibery/id",
                      Name: "fibery/name",
                      "Content Type": "fibery/content-type",
                      Secret: "fibery/secret",
                    },
                },
              Pain: "software-development/Pain",
            },
          "q/where": ["=", ["fibery/id"], "$id"],
          "q/limit": 1,
        },
        {"$id": "1"}),
      body: () => Promise.resolve([
        {
          "Fibery Id": "fibery/id",
          "param": "other/param",
          "Steps To Reproduce": {
            "Secret": "324d3bb1-740d-11e9-8f76-85b9cee696f7",
            "Fibery Id": "324d3bb0-740d-11e9-8f76-85b9cee696f7",
          },
        },
      ]),
    },
  ];

  test("via type id", async () => {
    let i = 0;
    const result = await performCreateOperation({
      api: makeApiMock(setupCalls()).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "2703295f-482a-4c49-944f-34961fc6503b",
          "CREATE:16ee3b91-b584-4662-97fd-2b9d134fc06f": "1. develop without bugs\n2. profit!",
          "CREATE:d78f224c-a85a-4335-bc6c-c7cd5468adf0": "u",
          "CREATE:d3213627-55b3-4a66-8a8d-0280d9bcd133": ["a", "b"],
          "CREATE:8daaf0aa-78b6-4d5a-8385-e61ba1e53cff": [
            "https://example.com/engine/hydrate/id/xxx",
            "https://example.com/engine/hydrate/id/yyy",
          ],
        },
      },
    }, () => String(++i));
    return expect(result).toEqual({
      "Fibery Id": "fibery/id",
      "param": "other/param",
      "Steps To Reproduce": "1. develop without bugs\n2. profit!",
    });
  });

  test("via type name", async () => {
    let i = 0;
    const result = await performCreateOperation({
      api: makeApiMock(setupCalls()).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "software-development/bug",
          "CREATE:16ee3b91-b584-4662-97fd-2b9d134fc06f": "1. develop without bugs\n2. profit!",
          "CREATE:d78f224c-a85a-4335-bc6c-c7cd5468adf0": "u",
          "CREATE:d3213627-55b3-4a66-8a8d-0280d9bcd133": ["a", "b"],
          "CREATE:8daaf0aa-78b6-4d5a-8385-e61ba1e53cff": [
            "https://example.com/engine/hydrate/id/xxx",
            "https://example.com/engine/hydrate/id/yyy",
          ],
        },
      },
    }, () => String(++i));
    return expect(result).toEqual({
      "Fibery Id": "fibery/id",
      "param": "other/param",
      "Steps To Reproduce": "1. develop without bugs\n2. profit!",
    });
  });

  test("via field names", async () => {
    let i = 0;
    const result = await performCreateOperation({
      api: makeApiMock(setupCalls()).proxy,
      bundle: {
        authData: {host: "host"},
        inputData: {
          type: "2703295f-482a-4c49-944f-34961fc6503b",
          "CREATE:software-development/steps-to-reproduce": "1. develop without bugs\n2. profit!",
          "CREATE:software-development/user-story": "u",
          "CREATE:assignments/assignees": ["a", "b"],
          "CREATE:fibery/files": [
            "https://example.com/engine/hydrate/id/xxx",
            "https://example.com/engine/hydrate/id/yyy",
          ],
        },
      },
    }, () => String(++i));
    return expect(result).toEqual({
      "Fibery Id": "fibery/id",
      "param": "other/param",
      "Steps To Reproduce": "1. develop without bugs\n2. profit!",
    });
  });
});
