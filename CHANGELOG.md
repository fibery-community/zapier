## 0.9.11

* allows upload files from Fibery to External Systems. Zapier docs: [Send files in Zaps](https://help.zapier.com/hc/en-us/articles/8496288813453-Send-files-in-Zaps#h_01J0P4GMX247E83K8B4WG591RA)

## 0.9.9

* fix generateUUIS is not a function

## 0.9.8

* typo

## 0.9.7

* move api to work with documents from core directly to /api/documents
* use nodejs randomUUID instead of UUID package
* update fibery schema package

## 0.9.3

* Support latest Fibery Platform
