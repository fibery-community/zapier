const _ = require("lodash");
const {createOrUpdateObject} = require("../../utils");
const {randomUUID} = require("node:crypto");

function performUpdateOperation({api, bundle: {inputData}}, generateUUID = randomUUID) {
  const {type, id} = inputData;
  if (!type) {
    throw new Error("bundle.inputData.type must be provided");
  }
  if (!id) {
    throw new Error("bundle.inputData.id must be provided");
  }
  const data = _.omit(inputData, "type", "id");

  return createOrUpdateObject({api, type, id, data}, generateUUID);
}

module.exports = {performUpdateOperation};
