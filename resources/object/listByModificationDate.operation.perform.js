const {
  getSelectWithSupportedFields,
  findTypeObjectByIdOrName,
  provideZapierId,
  withRichTextFieldsDocumentsLoaded,
  withObjectLink,
  withDehydratedFiles,
} = require("../../utils");

const PAGE_LIMIT = 100;

const single = (items, itemKind) => {
  if (items.length !== 1) {
    throw new Error(`Expected exactly 1 ${itemKind}, but found ${items.length}`);
  }
  return items[0];
};

async function performListByModificationDateOperation({api, bundle: {inputData: {type}, meta: {page}}}) {
  const schema = await api.fetchSchema();

  if (!type) {
    throw new Error("bundle.inputData.type must be provided");
  }
  const typeObject = findTypeObjectByIdOrName(schema.typeObjects, type);
  const idFieldObject = typeObject.idFieldObject;

  const modificationDateFieldObject = single(typeObject.fieldObjects.filter(
    (x) => x.rawMeta["fibery/modification-date?"]), `modification date field in ${type} type`);

  const objects = await api.queryObjects({
    "q/from": typeObject.name,
    "q/select": getSelectWithSupportedFields(schema.typeObjects, type),
    "q/order-by": [
      [[modificationDateFieldObject.name], "q/desc"],
      [[idFieldObject.name], "q/asc"],
    ],
    "q/limit": PAGE_LIMIT,
    "q/offset": PAGE_LIMIT * page,
  });

  const objectsWithDocuments = await withRichTextFieldsDocumentsLoaded(objects, typeObject, api);

  const objectsWithLinks = withObjectLink(objectsWithDocuments, typeObject, api).map((object) => {
    return provideZapierId({
      object,
      typeObject,
      mapId: (id) => `${id}@${object[modificationDateFieldObject.title]}`,
    });
  });

  return withDehydratedFiles(objectsWithLinks, typeObject, api);
}

module.exports = {performListByModificationDateOperation};
