const rawTypeObjects = require("./software-dev.types");
const fiberySchema = require("../../fibery-schema");

module.exports = fiberySchema.factory.makeSchema({
  "fibery/types": rawTypeObjects,
  "fibery/meta": {"fibery/version": "test"},
}, {shouldRemoveDeletedTypesAndFields: false});
