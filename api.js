const _ = require("lodash");
const util = require("util");
const fiberySchema = require("./fibery-schema");
const stashFileFunction = require("./resources/file/stashFileFunction");

const inspect = (object) => util.inspect(object, {
  depth: null,
  maxArrayLength: null,
});

const defaultFormat = "md";

const makeApi = (z, fiberyHost) => ({
  fetchJson({method, url, body}) {
    z.console.log(inspect({request: {method, url, body}}));
    return z.request({
      method,
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json; charset=utf-8",
      },
      body,
    }).then((response) => {
      const status = response.status;
      const body = response.json;
      z.console.log(inspect({response: {status, body, method, url}}));
      if (status !== 200) {
        throw new Error(body && body.message ? body.message : "HTTP " + status);
      }
      return body;
    });
  },

  stashFile(fileSecret) {
    if (!fileSecret) {
      return null;
    }

    return z.dehydrateFile(stashFileFunction, {
      fileUrl: `https://${fiberyHost}/api/files/${fileSecret}`,
    });
  },

  getFiberyHost() {
    return fiberyHost;
  },

  queryObjects(query, params) {
    return this.executeSingleCommand({command: "fibery.entity/query", args: {query, params}});
  },

  fetchSchema() {
    return this.executeSingleCommand({command: "fibery.schema/query", args: null})
      .then((rawSchema) => fiberySchema.factory.makeSchema(rawSchema));
  },

  fetchMe() {
    return this.queryObjects({
      "q/from": "fibery/user",
      "q/limit": "q/no-limit",
      "q/select": {"fibery/public-id": "fibery/public-id", "user/name": "user/name"},
      "q/where": ["=", ["fibery/id"], "$my-id"],
    }).then((items) => items.length === 0
      ? Promise.reject(new Error("Authenticated user does not exist"))
      : items[0]);
  },

  executeSingleCommand(command) {
    return this.fetchJson({
      method: "POST",
      url: `https://${fiberyHost}/api/commands`,
      body: [command],
    })
      .then((x) => x[0])
      .then(({success, result}) => {
        if (!success) {
          throw new Error(result.message);
        }
        return result;
      });
  },

  executeDocumentsCommand(command, format = defaultFormat) {
    return this.fetchJson({
      method: "POST",
      url: `https://${fiberyHost}/api/documents/commands?format=${format}`,
      body: command,
    });
  },

  async executeBatch(commands) {
    const results = await this.executeSingleCommand({
      command: "fibery.command/batch",
      args: {commands}
    });
    return results.map(({success, result}) => {
      if (!success) {
        throw new Error(result.message);
      }
      return result;
    });
  },

  async create(args) {
    if (args.hasOwnProperty("query")) {
      const results = await this.executeBatch([
        {command: "fibery.entity/create", args: _.omit(args, ["query", "params"])},
        {command: "fibery.entity/query", args: _.pick(args, ["query", "params"])},
      ]);
      return results[1];
    }
    return await this.executeSingleCommand({command: "fibery.entity/create", args});
  },

  async update(args) {
    if (args.hasOwnProperty("query")) {
      const results = await this.executeBatch([
        {command: "fibery.entity/update", args: _.omit(args, ["query", "params"])},
        {command: "fibery.entity/query", args: _.pick(args, ["query", "params"])},
      ]);
      return results[1];
    }
    return this.executeSingleCommand({command: "fibery.entity/update", args});
  },

  addCollectionItems(type, id, field, affectedIds) {
    const args = {
      type,
      entity: {"fibery/id": id},
      field,
      items: affectedIds.map((id) => ({"fibery/id": id})),
    };
    return this.executeSingleCommand({command: "fibery.entity/add-collection-items", args});
  },

  uploadFile(url) {
    return this.fetchJson({
      method: "POST",
      url: `https://${fiberyHost}/api/files/from-url`,
      body: {url},
    }).then((result) => {
        const fileId = result["fibery/id"];
        if (!fileId) {
          throw new Error("api/files/from-url must return fibery/id");
        }
        return fileId;
      });
  },

  async updateCollaborationDocuments(secretContentPairs) {
    return this.executeDocumentsCommand({
      command: "create-or-update-documents",
      args: secretContentPairs,
    });
  },

  async queryCollaborationDocuments(secrets) {
    return this.executeDocumentsCommand({
      command: "get-documents",
      args: secrets.map((secret) => ({secret})),
    });
  },
});

module.exports = makeApi;
