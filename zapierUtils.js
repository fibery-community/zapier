function exportExceptionAsZapierField(fn) {
  return function exceptionHandler() {
    try {
      const result = fn.apply(this, arguments);
      if (result.then) {
        return result.catch((e) => {
          return {
            key: "error",
            label: e ? e.message + e.stack : JSON.stringify(e) || "no async error",
          };
        });
      }
      return result;
    } catch (e) {
      return {
        key: "error",
        label: e ? e.message + e.stack : JSON.stringify(e) || "no sync error",
      };
    }
  };
}

module.exports = {exportExceptionAsZapierField};
