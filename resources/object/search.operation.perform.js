const {
  getSelectWithSupportedFields,
  getSupportedFieldObjects,
  toFiberyApiValue,
  findTypeObjectByIdOrName,
  provideZapierId,
  withRichTextFieldsDocumentsLoaded,
  withObjectLink,
  findByIdOrNamePredicate, withDehydratedFiles
} = require("../../utils");
const _ = require("lodash");

const FIELD_KEY_PREFIX = "SEARCH:";

function performSearchOperation({api, bundle: {inputData}}) {
  const {type} = inputData;
  if (!type) {
    throw new Error("bundle.inputData.type must be provided");
  }
  const rest = _.omit(inputData, "type");
  const rawSearchInputData = _.pickBy(rest, (value, key) => key.indexOf(FIELD_KEY_PREFIX) >= 0);
  const data = _.flow([
    _.toPairs,
    (pairs) => _.map(pairs, ([key, value]) => {
      return [key.replace(FIELD_KEY_PREFIX, ""), value];
    }),
    _.fromPairs,
  ])(rawSearchInputData);

  return api.fetchSchema().then((schema) => {
    const typeObject = findTypeObjectByIdOrName(schema.typeObjects, type);
    const supportedFieldObjects = getSupportedFieldObjects(typeObject);
    const pairsToSubmit = _.toPairs(data).map(([key, value]) => {
      const fieldObject = supportedFieldObjects.find(findByIdOrNamePredicate(key));
      if (!fieldObject) {
        throw new Error(`field "${key}" creation is not implemented`);
      }
      const paramRef = "$" + key.replace("/", "$");
      return {fieldObject, paramRef, key, value: toFiberyApiValue({type: fieldObject.type, value})};
    });
    const params = pairsToSubmit.reduce((memo, {paramRef, value}) => {
      memo[paramRef] = value;
      return memo;
    }, {});
    const whereClauses = pairsToSubmit.map(({paramRef, fieldObject: {name, typeObject, kind}}) => {
      if (kind === ":field/basic") {
        return ["=", [name], paramRef];
      }
      if (kind === ":field/reference") {
        return ["=", [name, typeObject.idFieldObject.name], paramRef];
      }
      if (kind === ":field/reference-collection") {
        return ["in", [name, typeObject.idFieldObject.name], paramRef];
      }
      throw new Error("Not implemented kind: " + kind);
    });
    if (whereClauses.length === 0) {
      throw new Error("At least one filter value must be present");
    }
    return api.queryObjects({
      "q/from": typeObject.name,
      "q/limit": "q/no-limit",
      "q/select": getSelectWithSupportedFields(schema.typeObjects, type),
      "q/where": ["and"].concat(whereClauses),
    }, params)
      .then((objects) => withRichTextFieldsDocumentsLoaded(objects, typeObject, api))
      .then((objects) => withObjectLink(objects, typeObject, api).map((object) => provideZapierId({object, typeObject})))
      .then((objects) => withDehydratedFiles(objects, typeObject, api));
  });
}

module.exports = {performSearchOperation, FIELD_KEY_PREFIX};
