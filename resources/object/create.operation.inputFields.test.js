const {retrieveTypeDependantFields} = require("./create.operation.inputFields");
const schema = require("./software-dev.schema");

describe("retrieveTypeDependantFields", () => {
  it("handles missing type value", async () => {
    const result = await retrieveTypeDependantFields({bundle: {inputData: {}}});
    return expect(result).toEqual([]);
  });

  it("handles invalid type value", () => {
    return expect(retrieveTypeDependantFields({
      api: {fetchSchema: () => Promise.resolve({typeObjects: []})},
      bundle: {inputData: {type: "software-development/user-story"}},
    })).rejects.toEqual(new Error(`Type "software-development/user-story" does not exist`));
  });

  it("handles soft deleted type value", () => {
    let requestId = 0;
    return expect(retrieveTypeDependantFields({
      api: {
        fetchSchema: () => Promise.resolve(schema),
        queryObjects: (body) => {
          const title = JSON.stringify({requestId: ++requestId, body});
          return Promise.resolve([
            {
              "fibery/id": "ID",
              "user/name": title,
              "software-development/name": title,
            },
          ]);
        },
      },
      bundle: {inputData: {type: "software-development/bug_qde4d34_deleted"}},
    })).rejects.toEqual(new Error(`Type "software-development/bug_qde4d34_deleted" does not exist`));
  });

  it("retrieves fields by type name", async () => {
    let requestId = 0;
    const typeDependantFields = await retrieveTypeDependantFields({
      api: {
        fetchSchema: () => Promise.resolve(schema),
        queryObjects: (body) => {
          const title = JSON.stringify({requestId: ++requestId, body});
          return Promise.resolve([
            {
              "fibery/id": "ID",
              "user/name": title,
              "software-development/name": title,
            },
          ]);
        },
      },
      bundle: {inputData: {type: "software-development/user-story"}},
    });
    return expect(typeDependantFields).toMatchSnapshot();
  });

  it("retrieves fields by type id", async () => {
    let requestId = 0;
    const typeDependantFields = await retrieveTypeDependantFields({
      api: {
        fetchSchema: () => Promise.resolve(schema),
        queryObjects: (body) => {
          const title = JSON.stringify({requestId: ++requestId, body});
          return Promise.resolve([
            {
              "fibery/id": "ID",
              "user/name": title,
              "software-development/name": title,
            },
          ]);
        },
      },
      bundle: {inputData: {type: "94bc6355-9cc1-4e69-b99c-826f7fcbea33"}},
    });
    return expect(typeDependantFields).toMatchSnapshot();
  });

  it("retrieves fields for deleted type by type id", async () => {
    let requestId = 0;
    return expect(retrieveTypeDependantFields({
      api: {
        fetchSchema: () => Promise.resolve(schema),
        queryObjects: (body) => {
          const title = JSON.stringify({requestId: ++requestId, body});
          return Promise.resolve([
            {
              "fibery/id": "ID",
              "user/name": title,
              "software-development/name": title,
            },
          ]);
        },
      },
      bundle: {inputData: {type: "440aea04-f3b2-4625-8d4c-06322ac75cab"}},
    })).rejects.toEqual(new Error(`Type "440aea04-f3b2-4625-8d4c-06322ac75cab" does not exist`));
  });

  it("retrieves fields bug (with collab doc field)", async () => {
    let requestId = 0;
    const typeDependantFields = await retrieveTypeDependantFields({
      api: {
        fetchSchema: () => Promise.resolve(schema),
        queryObjects: (body) => {
          const title = JSON.stringify({requestId: ++requestId, body});
          return Promise.resolve([
            {
              "fibery/id": "ID",
              "user/name": title,
              "software-development/name": title,
            },
          ]);
        },
      },
      bundle: {inputData: {type: "software-development/bug"}},
    });
    return expect(typeDependantFields).toMatchSnapshot();
  });
});
