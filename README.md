Zapier connector to Fibery


Use nodejs compatible with zapier-platform see version in their api references.
```
Why doesn't Zapier support newer versions of Node.js?
We run your code on AWS Lambda, which only supports a few versions of Node (the latest of which is v14. As that updates, so too will we.
```

Login to zapier-cli using credentials in keePass.

Make your changes, update version in `package.json` then execute
```
zapier push
```
