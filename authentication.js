const {toManagedFunction, getFiberyHost} = require("./utils");

module.exports = {
  type: "oauth2",
  oauth2Config: {
    authorizeUrl: {
      method: "GET",
      url:
        "https://auth.fibery.io/oauth2/auth",
      params: {
        client_id: "{{process.env.CLIENT_ID}}",
        state: "{{bundle.inputData.state}}",
        redirect_uri: "{{bundle.inputData.redirect_uri}}",
        response_type: "code",
      },
    },
    getAccessToken: {
      method: "POST",
      url: "https://auth.fibery.io/oauth2/token",
      body: {
        code: "{{bundle.inputData.code}}",
        client_id: "{{process.env.CLIENT_ID}}",
        client_secret: "{{process.env.CLIENT_SECRET}}",
        redirect_uri: "{{bundle.inputData.redirect_uri}}",
        grant_type: "authorization_code",
      },
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    },
    refreshAccessToken: {
      method: "POST",
      url: "https://auth.fibery.io/oauth2/token",
      body: {
        client_id: "{{process.env.CLIENT_ID}}",
        client_secret: "{{process.env.CLIENT_SECRET}}",
        refresh_token: "{{bundle.authData.refresh_token}}",
        grant_type: "refresh_token",
      },
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    },
    scope: "openid offline",
    autoRefresh: true,
  },
  fields: [
    {key: "id_token", type: "string", required: false, computed: true},
  ],
  test: toManagedFunction(({api}) => api.fetchMe()
    .then(({"fibery/public-id": publicId, "user/name": title}) => ({publicId, title}))),
  connectionLabel: (z, bundle) => {
    const fiberyHost = getFiberyHost(bundle);
    return `${bundle.inputData.title} (User ${bundle.inputData.publicId}) @ ${fiberyHost}`;
  },
};
