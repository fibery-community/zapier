const makeApi = require("./api");
const _ = require("lodash");
const jwt = require("jsonwebtoken");

const collabDocType = "Collaboration~Documents/Document";
const secretKey = "Secret";

function getReferenceSelect({typeObject, wrapPath}) {
  return _.fromPairs(typeObject.fieldObjects
    .filter(isSupportedBasicFieldObject)
    .filter((fieldObject) => fieldObject.isId || fieldObject.kind === ":field/basic")
    .map((fieldObject) => [
      fieldObject.isId ? fiberyIdFieldName : fieldObject.title,
      wrapPath(fieldObject.name),
    ]));
}

const provideZapierId = ({object, typeObject, mapId = (id) => id}) => {
  const id = object[fiberyIdFieldName];
  if (!id) {
    throw new Error(`object does not have ${fiberyIdFieldName} field`);
  }
  return Object.assign({id: mapId(id)}, object);
};

const fiberyIdFieldName = "Fibery Id";

const getSelectWithSupportedFields = (typeObjects, type, selectFields = () => true) => {
  const typeObject = findTypeObjectByIdOrName(typeObjects, type);
  const fieldObjects = filterSupportedFieldObjects(typeObject.fieldObjects.filter(selectFields));
  return fieldObjects.reduce((select, {isId, kind, title, name, typeObject}) => {
    if (isId) {
      select[fiberyIdFieldName] = name;
      return select;
    }
    if (kind === ":field/basic") {
      select[title] = name;
      return select;
    }
    if (kind === ":field/reference") {
      select[title] = getReferenceSelect({typeObject, wrapPath: (x) => [name, x]});
      return select;
    }
    if (kind === ":field/reference-collection") {
      select[title] = {
        "q/from": [name],
        "q/limit": "q/no-limit",
        "q/select": getReferenceSelect({typeObject, wrapPath: _.identity}),
      };
      return select;
    }
    throw new Error(kind + " field kind is not implemented");
  }, {});
};

//TODO: add mapping for date range fields
const fiberyToZapierTypesMap = {
  "fibery/bool": "boolean",
  "fibery/date": "datetime",
  "fibery/date-time": "datetime",
  "fibery/email": "string",
  "fibery/emoji": "string",
  "fibery/rich-text": "text",
  "fibery/text": "text",
  "fibery/url": "text",
  "fibery/int": "number",
  "fibery/decimal": "number",
  "fibery/uuid": "string",
  "fibery/file": "file",
  [collabDocType]: "text",
};

function findByIdOrNamePredicate(key) {
  return (obj) => obj.id === key || obj.name === key;
}


const findTypeObjectByIdOrName = (typeObjects, idOrName) => {
  const typeObject = typeObjects.find(findByIdOrNamePredicate(idOrName));

  if (!typeObject || typeObject.isDeleted) {
    throw new Error(`Type "${idOrName}" does not exist`);
  }
  return typeObject;
};

const getSupportedFieldObjects = (typeObject) => {
  return filterSupportedFieldObjects(typeObject.fieldObjects);
};

const UNSUPPORTED_TYPES = new Set(["fibery/rich-text", "fibery/rank", "fibery/Button", "fibery/view"]);
const filterSupportedFieldObjects = (fieldObjects) => {
  return fieldObjects.filter((fieldObject) => !fieldObject.isDeleted && !UNSUPPORTED_TYPES.has(fieldObject.type));
};

function resolveZapierType(fieldObject) {
  return fiberyToZapierTypesMap[fieldObject.type] || null;
}

function isSupportedBasicFieldObject(fieldObject) {
  const type = fieldObject.type;
  return !fieldObject.isDeleted && fieldObject.type !== "fibery/rich-text" && (type in fiberyToZapierTypesMap);
}

const fetchChoices = ({api, typeObject}) => {
  const idFieldObject = typeObject.idFieldObject;
  const titleFieldObject = typeObject.titleFieldObject || idFieldObject;
  return api.queryObjects({
    "q/from": typeObject.name,
    "q/select": {
      [idFieldObject.name]: idFieldObject.name,
      [titleFieldObject.name]: titleFieldObject.name,
    },
    "q/limit": 500,
  }).then((objects) => objects.reduce((memo, object) => {
    const {[idFieldObject.name]: id, [titleFieldObject.name]: title} = object;
    memo[id] = title;
    return memo;
  }, {}));
};

const fetchObject = ({api, typeObjects, type, id}) => {
  const typeObject = findTypeObjectByIdOrName(typeObjects, type);

  return api.queryObjects({
    "q/from": typeObject.name,
    "q/select": getSelectWithSupportedFields(typeObjects, type),
    "q/where": ["=", [typeObject.idFieldObject.name], "$id"],
    "q/limit": 1,
  }, {$id: id}).then((objects) => {
    const object = objects[0];
    if (!object) {
      throw new Error(`Unable to find object of type ${type} with id ${id}`);
    }
    return object;
  });
};

const isRangeType = (type) => type === "fibery/date-range" || type === "fibery/date-time-range";

const rangeStartKeyPostfix = "_start";
const rangeEndKeyPostfix = "_end";

const loadField = ({z, api, includeChoices, fieldObject, fieldKeyPrefix}) => {
  const keyPrefix = fieldKeyPrefix || "";
  const fieldObjectId = `${keyPrefix}${fieldObject.id}`;
  const fieldZapierType = resolveZapierType(fieldObject);
  if (isRangeType(fieldObject.type)) {
    return Promise.resolve([
      {
        key: `${fieldObjectId}${rangeStartKeyPostfix}`,
        type: "datetime",
        label: `${fieldObject.title} Start`,
      },
      {
        key: `${fieldObjectId}${rangeEndKeyPostfix}`,
        type: "datetime",
        label: `${fieldObject.title} End`,
      },
    ]);
  }
  if (fieldObject.kind === ":field/basic" || fieldObject.type === collabDocType) {
    if (!fieldZapierType) {
      z.console.log(`Zapier type for ${fieldObject.name} is not defined`);
      return Promise.resolve([]);
    }
    return Promise.resolve([
      {
        key: fieldObjectId,
        type: fieldZapierType,
        label: fieldObject.isId ? fiberyIdFieldName : fieldObject.title,
      },
    ]);
  }
  if (fieldObject.kind === ":field/reference" || fieldObject.kind === ":field/reference-collection") {
    if (fieldObject.typeObject.name === "fibery/file") {
      return Promise.resolve([
        {
          key: fieldObjectId,
          type: fieldZapierType,
          label: fieldObject.title,
          list: fieldObject.kind === ":field/reference-collection",
        },
      ]);
    }
    const gotChoices = includeChoices
      ? fetchChoices({api, typeObject: fieldObject.typeObject})
      : Promise.resolve({});
    return gotChoices.then((choices) => {
      return [
        {
          key: fieldObjectId,
          type: resolveZapierType(fieldObject.typeObject.idFieldObject),
          label: fieldObject.title,
          list: fieldObject.kind === ":field/reference-collection",
          choices,
          search: "objectSearch.id",
        },
      ];
    });
  }
  throw new Error(`kind ${fieldObject.kind} is not implemented`);
};

const loadFields = ({z, typeObjects, type, api, includeChoices, fieldKeyPrefix, fieldFilterFn}) => {
  const typeObject = findTypeObjectByIdOrName(typeObjects, type);
  const typeFields = getSupportedFieldObjects(typeObject)
    .filter((field) => field.type !== `fibery/Button` && fieldFilterFn(field))
    .map((fieldObject) => loadField({
      z,
      api,
      includeChoices,
      fieldObject,
      fieldKeyPrefix,
    }));
  return Promise.all(typeFields).then(_.flatten);
};

const withApi = (fn) => (z, bundle) => fn({api: makeApi(z, getFiberyHost(bundle, z)), z, bundle});

const inspect = (obj) => require("util").inspect(obj);

const withIOLogging = (fn) => {
  const functionName = fn.name || "anonymous";
  return function logCallsWrapper({z, bundle}) {
    const callLabel = `call ${functionName}`;
    z.console.log(`${callLabel} with bundle arg=${inspect(bundle)}`);

    const result = fn.apply(this, arguments);
    if (!result.then) {
      z.console.log(`${callLabel} result=${inspect(result)}`);
      return result;
    }
    return result.then(
      (resolveValue) => {
        z.console.log(`${callLabel} resolveValue=${inspect(resolveValue)}`);
        return resolveValue;
      },
      (rejectValue) => {
        z.console.log(`${callLabel} rejectValue=${inspect(rejectValue)}`);
        return Promise.reject(rejectValue);
      },
    );
  };
};

const toManagedFunction = (fn) => withApi(withIOLogging(fn));

const toCalendarDateString = (value) => value ? value.split("T")[0] : value;
const toISODateString = (value, transform) => value ? transform(new Date(value)).toISOString() : value;

const zapierToFiberyValueFormatters = {
  "fibery/date": (value) => toCalendarDateString(value),
  "fibery/date-range": (value) => toCalendarDateString(value),
  "fibery/date-time": (value) => toISODateString(value, _.identity),
  "fibery/date-time-range": (value) => toISODateString(value, _.identity),
};

const toFiberyApiValue = ({type, value}) => {
  const format = zapierToFiberyValueFormatters[type] || _.identity;
  return format(value);
};

const createOrUpdateObject = async ({api, type, id, data}, generateUUID) => {
  const schema = await api.fetchSchema();
  const typeObject = findTypeObjectByIdOrName(schema.typeObjects, type);
  const supportedFieldObjects = getSupportedFieldObjects(typeObject);
  const pairsToSubmit = _.toPairs(data).map(([key, value]) => {
    const isRangeStart = key.endsWith(rangeStartKeyPostfix);
    const isRangeEnd = key.endsWith(rangeEndKeyPostfix);

    let fieldObject = supportedFieldObjects.find(findByIdOrNamePredicate(key));
    if (isRangeStart || isRangeEnd) {
      const postfixLength = isRangeStart ? rangeStartKeyPostfix.length : rangeEndKeyPostfix.length;
      fieldObject = supportedFieldObjects.find(
        findByIdOrNamePredicate(key.substr(0, key.length - postfixLength))
      );
    }

    if (!fieldObject) {
      throw new Error(`field "${key}" creation is not implemented`);
    }
    return {
      fieldObject,
      isRangeStart,
      isRangeEnd,
      value: toFiberyApiValue({type: fieldObject.type, value}),
    };
  });
  const [basicPairs, refPairs] = _.partition(pairsToSubmit, ({fieldObject: {kind}}) => kind === ":field/basic");
  const pairsGroupedByField = _.groupBy(basicPairs, ({fieldObject}) => fieldObject.name);
  const pairsWithDateRangesMerged = Object.values(pairsGroupedByField).map((group) => {
    if (isRangeType(group[0].fieldObject.type)) {
      return group.reduce((agg, x) => {
        const key = x.isRangeStart ? "start" : "end";
        return {fieldObject: x.fieldObject, value: {...agg.value, [key]: x.value}};
      }, {});
    }
    return group[0];
  });
  const object = pairsWithDateRangesMerged.reduce((memo, {fieldObject, value}) => {
    memo[fieldObject.name] = value;
    return memo;
  }, {});
  const idFieldName = typeObject.idFieldObject.name;
  object[idFieldName] = id ? id : generateUUID();

  const collabDocFilter = ((fieldObject) => fieldObject.type === collabDocType);
  let collabDocIndex = {};
  const documentPairs = refPairs.filter(({fieldObject}) => collabDocFilter(fieldObject));

  const modification = id ? api.update : api.create;

  const [modificationObject] = await modification.call(api, {
    type: typeObject.name,
    entity: object,
    query: {
      "q/from": typeObject.name,
      "q/select": {
        [idFieldName]: idFieldName,
        ...getSelectWithSupportedFields(schema.typeObjects, type, collabDocFilter)
      },
      "q/where": ["=", ["fibery/id"], "$id"],
      "q/limit": 1,
    },
    params: {"$id": object[idFieldName]},
  });

  const idEnsured = modificationObject[idFieldName];
  if (!idEnsured) {
    throw new Error("Create response must have id");
  }

  if (documentPairs.length > 0) {
    const collaborationDocuments = documentPairs.map(({fieldObject, value}) => {
      const fieldRef = modificationObject[fieldObject.title];
      const secret = fieldRef ? fieldRef[secretKey] : generateUUID();
      return {secret, content: value};
    });

    await api.updateCollaborationDocuments(collaborationDocuments);

    collabDocIndex = collaborationDocuments.reduce((index, {secret, content}) => {
      index[secret] = content;
      return index;
    }, collabDocIndex);
  }

  const promises = refPairs.filter(({fieldObject}) => !collabDocFilter(fieldObject))
    .map(({fieldObject, value}) => {
      const values = fieldObject.kind === ":field/reference-collection" ? value : [value];
      if (fieldObject.typeObject.name === "fibery/file") {
        return Promise
          .all(values.map((url) => api.uploadFile(url)))
          .then((ids) => api.addCollectionItems(typeObject.name, idEnsured, fieldObject.name, ids));
      }
      return api.addCollectionItems(typeObject.name, idEnsured, fieldObject.name, values);
    });

  await Promise.all(promises);

  const resultObject = await fetchObject({api, typeObjects: schema.typeObjects, type, id: idEnsured});
  return documentPairs.reduce((memo, {fieldObject}) => {
    const k = fieldObject.title;
    const collabDocRef = memo[k];
    const secret = collabDocRef ? collabDocRef[secretKey] : null;
    if (secret) {
      memo[k] = collabDocIndex[secret];
    }
    return memo;
  }, resultObject);
};

const urlAllowedChars = [
  `$`, `-`, `_`, `.`, `+`, `!`, `*`, `'`, `(`, `)`, `,`,
];

const escapeRegExpChar = (x) => `\\${x}`;
const urlAllowedCharsPatternGroup = `a-zA-Z0-9${urlAllowedChars.map(escapeRegExpChar).join("")}`;
const nonUrlChars = new RegExp(`[^${urlAllowedCharsPatternGroup}]+`, "g");

const makeLocator = ({publicId, title}) => {
  if (!publicId) {
    throw new Error("publicId is required");
  }
  const limit = 100;
  return [
    title && title.replace(nonUrlChars, " ")
      .trim()
      .replace(/ /g, "-")
      .substr(0, limit),
    publicId,
  ].filter(Boolean).join("-");
};

const getObjectLink = (object, typeObject, api) => {
  const path = `/${typeObject.name.replace(/ /g, "–")}/${makeLocator({
    publicId: object[typeObject.publicIdFieldObject.title],
    title: object[typeObject.titleFieldObject.title],
  })}`;
  return encodeURI(`https://${api.getFiberyHost()}${path}`);
};

const withObjectLink = (objects, typeObject, api) => {
  return objects.map((object) => ({...object, "Entity Link": getObjectLink(object, typeObject, api)}));
};

const withRichTextFieldsDocumentsLoaded = async (objects, typeObject, api) => {
  const docFieldKeys = typeObject.fieldObjects
    .filter((fieldObject) => fieldObject.type === collabDocType)
    .map(({title}) => title);

  const secrets = docFieldKeys.reduce((memo, k) => {
    return objects.reduce((memo, object) => {
      const collabDocRef = object[k];
      const secret = collabDocRef ? collabDocRef[secretKey] : null;
      return memo.concat(secret ? [secret] : []);
    }, memo);
  }, []);

  if (secrets.length === 0) {
    return objects;
  }
  const documents = await api.queryCollaborationDocuments(secrets);
  const documentsIndex = documents.reduce((memo, item) => {
    memo[item.secret] = item.content;
    return memo;
  }, {});

  return objects.map((object) => {
    return docFieldKeys.reduce((memo, k) => {
      const collabDocRef = object[k];
      const secret = collabDocRef ? collabDocRef[secretKey] : null;
      if (secret) {
        memo[k] = documentsIndex[secret];
      }
      return memo;
    }, object);
  });
};

const getFiberyHost = (bundle) => {
  const token = bundle.authData.access_token;
  const decoded = jwt.decode(token);
  return decoded.ext.hostname;
};

const withDehydratedFile = (api, fileTypeObject, fileObject) => {
  if (!fileObject) {
    return fileObject;
  }

  const fieldName = fileTypeObject.fieldObjectsByName["fibery/secret"].title;
  fileObject.File = api.stashFile(fileObject[fieldName]);
};

const withDehydratedFiles = (objects, typeObject, api) => {
  const fileFieldObjects = typeObject.fieldObjects.filter(({type}) => type === "fibery/file");

  if (fileFieldObjects.length === 0) {
    return objects;
  }

  objects.forEach((object) => {
    fileFieldObjects.forEach((fieldObject) => {
      const value = object && object[fieldObject.title];

      if (!value) {
        return;
      }

      const items = Array.isArray(value) ? value : [value];
      items.forEach((item) => withDehydratedFile(api, fieldObject.typeObject, item));
    });
  });

  return objects;
};

module.exports = {
  toManagedFunction,
  getSelectWithSupportedFields,
  getSupportedFieldObjects,
  loadFields,
  toFiberyApiValue,
  createOrUpdateObject,
  getFiberyHost,
  provideZapierId,
  withRichTextFieldsDocumentsLoaded,
  withObjectLink,
  collabDocType,
  secretKey,
  findTypeObjectByIdOrName,
  findByIdOrNamePredicate,
  resolveZapierType,
  isRangeType,
  fiberyIdFieldName,
  withDehydratedFiles,
};
