module.exports = {
    key: "type",
    noun: "Type",
    outputFields: [
        {key: "id", label: "Id"},
        {key: "title", label: "Title"},
    ],
    list: require("./list"),
};
